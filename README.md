# webrtc_load_test_webpack

This project is used for webrtc load testing of XiVO platform.

## dependencies

This project requires to have docker installed.

## description

The docker container serves custom, minimal webpages, that can establish an webrtc call with XiVO systems.
These pages are then loaded in client web browsers and passing the parameters (for login, or called numbers) is done via url.
