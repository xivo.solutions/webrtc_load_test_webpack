#!/bin/bash

function setup_resolving() {
    if [[ -n "${XIVOCC_IP}" ]]; then
        echo "Setting up resolving of $SUT_FQDN"
        echo "$XIVOCC_IP $SUT_FQDN" >> /etc/hosts
    fi
}

#setup_resolving

SUT_SOURCE=$(wget --no-check-certificate -O- https://$SUT_FQDN > /dev/stdout)

function create_dist_directory() {
    echo "Creating dist directory"
    if [ ! -d dist/scripts/ ];
    then
        mkdir -p dist/scripts/
    fi
}

function include_scripts_into_dist() {
    echo "Downloading scripts into dist directory"
    SCRIPT_URLS=($(grep -oP '\/assets\/javascripts\/.*\.js' <<< "$SUT_SOURCE"))
    for script_url in "${SCRIPT_URLS[@]}"
    do
        wget -N https://$SUT_FQDN$script_url -P dist/scripts/ --no-check-certificate
    done
}

function include_scripts_into_index() {
    echo "Including scripts into index.html"
    sed -i '/<script.*/d' index.html
    SCRIPT_FILENAMES=($(grep -oP '(\/assets\/javascripts\/)(.*\/)?\K(.*\.js)' <<< "$SUT_SOURCE"))
    for script_filename in "${SCRIPT_FILENAMES[@]}"
    do  
        sed -i "/<\/head>/i<script src=./scripts/$script_filename><\/script>" index.html
    done
}

function edit_xc_webrtc(){
    echo "Editing xc_webrtc.js"
    local xc_webrtc_filename=$(ls dist/scripts/ | grep "xc_webrtc")
    sed -i '/holdConnectedCalls();/d' ./dist/scripts/$xc_webrtc_filename
}

function edit_sipml(){
    echo "Editing SIPml-api.min.js"
    local sipml_filename=$(ls dist/scripts/ | grep "SIPml-api.min.js")
    sed -i 's/sdpSemantics:"unified-plan"/sdpSemantics:"unified-plan",bundlePolicy:"max-bundle"/' ./dist/scripts/$sipml_filename
}

function run_npm() {
    echo "Runinng npm"
    npm install
    npx jasmine init
    npm run compile
    npm run build 
    npm run start 
}

create_dist_directory
include_scripts_into_dist
include_scripts_into_index
edit_xc_webrtc
run_npm 