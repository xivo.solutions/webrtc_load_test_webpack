const HTTPS = 'https://';
const WSS   = 'wss://';

export const httpsAuthUrl           = `${HTTPS}${process.env.SUT_FQDN}/xuc/api/2.0/auth/login`;
export const audioTag               = 'audio_remote';
export const wssAuthUrl             = `${WSS}${process.env.SUT_FQDN}/xuc/api/2.0/cti?token=`;
export const port                   = '443';
export const ssl                    = true;
export const ringingEvent           = 'Setup';
export const callTerminatedEvent    = 'Terminated';
export const xucUrl                 = process.env.SUT_FQDN;
export const proxyPort              = process.env.CLIENT_PROXY_PORT;
export const loadtestUrl            = process.env.LOADTEST_URL;
export const xivoIp                 = process.env.XIVO_IP;
export const mds1Ip                 = process.env.MDS1_IP;
export const mds2Ip                 = process.env.MDS2_IP;
