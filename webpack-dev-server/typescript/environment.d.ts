export { }

declare global {
    namespace NodeJS {
        interface ProcessEnv {
            SUT_FQDN: string,
            WEBPACK_PORT: string,
            DEFAULT_USER_PWD: string,
            CLIENT_PROXY_PORT: string,
            LOADTEST_URL: string,
            XIVO_IP: string,
            MDS1_IP: string,
            MDS2_IP: string,
            SCENARIOS: string,
        }
    }
}