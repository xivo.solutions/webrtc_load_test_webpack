import { User } from '../../lib/User';
import { initScenarioUserDataFromURL, initOrIncrementIteration, getLineConfigForCommonPeer } from '../../lib/LoadTestUtils';
import { xcWebrtcEvent } from '../../main';

const urlQuery = [
    "username",
    "userType",
    "userNumber",
    "partyNumber",
    "calleeCommonPeerId",
    "calleeCommonPeerName",
    "calleeCommonPeerNumber",
];
const {
    username, userType, userNumber, partyNumber, calleeCommonPeerId,
    calleeCommonPeerName, calleeCommonPeerNumber
} = initScenarioUserDataFromURL(urlQuery);

declare var window;
declare var Cti;
declare var xc_webrtc;

initOrIncrementIteration();
window['appVersion'] = `${userType}.${username}`;
let unsetRegistrationHandler;
let unsetIncomingCallHandler;

function loadCallee(): void {
    Cti.setHandler(Cti.MessageType.LOGGEDON, () => startRegistration());
    (new User(username, userNumber)).login();
    return;
}

function startRegistration(): void {
    Cti.unsetHandler(Cti.MessageType.LOGGEDON, startRegistration);
    console.log(`xc_webrtc.registrationHandler: waiting for ${calleeCommonPeerName} registration.`);
    unsetRegistrationHandler = xc_webrtc.setHandler(xc_webrtc.MessageType.REGISTRATION, finishRegistration);
    getLineConfigForCommonPeer(calleeCommonPeerId, calleeCommonPeerName, calleeCommonPeerNumber);
    return;
};

function finishRegistration(e): void {
    console.log(`xc_webrtc.registrationHandler: ${JSON.stringify(e)}`);
    if (e.type === 'Registered') {
        console.log(`xc_webrtc.registrationHandler: finished registration.`);
        unsetRegistrationHandler();
        unsetIncomingCallHandler = xc_webrtc.setHandler(xc_webrtc.MessageType.INCOMING, onRing);
        return;
    }
    return;
}

function onRing(e: xcWebrtcEvent): void {
    console.log(`xc_webrtc.incomingCallHandler: ${JSON.stringify(e)}`);
    if (e.type === "Setup") {
        console.log(`xc_webrtc.incomingCallHandler: incoming call, answering.`);
        setTimeout(() => {
            xc_webrtc.answer();
        }, 2000);
        return;
    }
    return;
}

// function hangupAfterTimeout(): void {
//     setTimeout(() => {
//         Cti.setHandler(Cti.MessageType.PHONEEVENT, () => closeLine());
//         xc_webrtc.hangup();
//         return;
//     }, parseInt(callLenght));
// }

// function closeLine(): void {
//     Cti.unsetHandler(Cti.MessageType.PHONEEVENT, closeLine);
//     console.log('PhoneEvent: Hangup, going to close line.');
//     // xc_webrtc.stop();
//     return;
// }

loadCallee();