import { User } from '../../lib/User';
import { initScenarioUserDataFromURL, initOrIncrementIteration, getLineConfigForCommonPeer } from '../../lib/LoadTestUtils';

const urlQuery = [
    "username",
    "userType",
    "userNumber",
    "partyNumber",
    "callerCommonPeerId",
    "callerCommonPeerName",
    "callerCommonPeerNumber",
    "delayBetweenCalls",
];
const {
    username, userType, userNumber, partyNumber, callerCommonPeerId,
    callerCommonPeerName, callerCommonPeerNumber, delayBetweenCalls
} = initScenarioUserDataFromURL(urlQuery);

declare var window;
declare var Cti;
declare var xc_webrtc;

initOrIncrementIteration();
window['appVersion'] = `${userType}.${userNumber}`;
let unsetRegistrationHandler; 
let unsetIncomingCallHandler;

function loadCaller(): void {
    Cti.setHandler(Cti.MessageType.LOGGEDON, () => startRegistration());
    (new User(username, userNumber)).login();
    return;
}

function startRegistration(): void {
    Cti.unsetHandler(Cti.MessageType.LOGGEDON, startRegistration);
    console.log(`xc_webrtc.registrationHandler: waiting for ${callerCommonPeerName} registration. userId ${callerCommonPeerId}`);
    unsetRegistrationHandler = xc_webrtc.setHandler(xc_webrtc.MessageType.REGISTRATION, finishRegistration);
    getLineConfigForCommonPeer(callerCommonPeerId, callerCommonPeerName, callerCommonPeerNumber);
    return;
};

function finishRegistration(e): void {
    console.log(`xc_webrtc.registrationHandler: ${JSON.stringify(e)}`);
    if (e.type === 'Registered') {
        console.log(`xc_webrtc.registrationHandler: finished registration.`);
        unsetRegistrationHandler();
        xc_webrtc.setHandler(xc_webrtc.MessageType.OUTGOING, makeAnotherCall);
        dialCalleeAfterDelay();
        return;
    }
    return;
}

function makeAnotherCall(e): void {
    console.log(`xc_webrtc.outgoingCallHandler: ${JSON.stringify(e)}`);
    if (e.type === "Connected") {
        console.log(`xc_webrtc.outgoingCallHandler: call established, dialing again.`);
        setTimeout(() => {
            dialCalleeAfterDelay();
        }, delayBetweenCalls);
    }
}

function dialCalleeAfterDelay(): void {
    // Cti.setHandler(Cti.MessageType.PHONEEVENT, (ev: PhoneEvent) => whenCalleeHangup(ev));
    setTimeout(() => {
        xc_webrtc.dial(partyNumber);
    }, delayBetweenCalls);
}

// function whenCalleeHangup(e: PhoneEvent): void {
//     if (e.eventType === 'EventReleased') {
//         Cti.unsetHandler(Cti.MessageType.PHONEEVENT, whenCalleeHangup);
//         console.log('PhoneEvent: Callee hangup, going to hangup too and close line.');
//         xc_webrtc.stop();
//         return;
//     }
//     return;
// };

loadCaller();