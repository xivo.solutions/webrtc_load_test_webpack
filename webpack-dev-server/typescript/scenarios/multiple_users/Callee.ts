import { User } from '../../lib/User';
import { PhoneHintStatusEvent, PhoneEvent } from '../../main';
import { initScenarioUserDataFromURL, initOrIncrementIteration, getLineConfig, attachReporters } from '../../lib/LoadTestUtils';

const urlQuery = [
    "username",
    "userType",
    "userNumber",
    "partyNumber",
    "callLength",
];
const {
    username, userType, userNumber,
    partyNumber, callLength }
= initScenarioUserDataFromURL(urlQuery);

declare var window;
declare var Cti;
declare var xc_webrtc;

initOrIncrementIteration();
window['appVersion'] = `${userType}.${username}`
window.onbeforeunload = function(){
    xc_webrtc.stop();
    localStorage.clear();
}

function loadCallee(): void {
    Cti.setHandler(Cti.MessageType.LOGGEDON, () => startRegistration());
    (new User(username, userNumber)).login();
    return;
}

function startRegistration(): void {
    Cti.unsetHandler(Cti.MessageType.LOGGEDON, startRegistration);
    console.log(`PhoneStatusUpdate: Setting handler for own phone ${userNumber}`);
    Cti.setHandler(Cti.MessageType.PHONEHINTSTATUSEVENT, (e: PhoneHintStatusEvent) => finishRegistration(e));
    attachReporters(username, userNumber, userType, partyNumber);
    Cti.subscribeToPhoneHints([userNumber]);
    getLineConfig(username);
    return;
};

function finishRegistration(e: PhoneHintStatusEvent): void {
    if (e.status === 0 && e.number === userNumber) {
        Cti.unsetHandler(Cti.MessageType.PHONEHINTSTATUSEVENT, finishRegistration);
        Cti.unsubscribeFromAllPhoneHints();
        console.log(`PhoneStatusUpdate: Own phone ${userNumber} ready - event.number: ${e.number} has event.status: ${e.status}`);
        Cti.setHandler(Cti.MessageType.PHONEEVENT, (e: PhoneEvent) => onRing(e));
        return;
    }
    return;
}

function onRing(e: PhoneEvent): void {
    if (e.eventType === 'EventRinging') {
        Cti.unsetHandler(Cti.MessageType.PHONEEVENT, onRing);
        console.log('PhoneEventHandler: Ringing, going to answer.');
        Cti.answer();
        hangupAfterTimeout();
        return;
    }
    return;
}

// Callee should remain registered after hangup, so that caller can dial him immediately after he reloads.
function hangupAfterTimeout(): void {
    console.log(`Hanging up after ${callLength} ms.`);
    setTimeout(() => {
        console.log(`Hanging up now.`);
        Cti.hangup();
        xc_webrtc.stop();
        return;
    }, callLength);
}

loadCallee();