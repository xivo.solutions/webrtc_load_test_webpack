import { User } from '../../lib/User';
import { PhoneHintStatusEvent, PhoneEvent } from '../../main';
import { initScenarioUserDataFromURL, initOrIncrementIteration, getLineConfig, attachReporters } from '../../lib/LoadTestUtils';

const urlQuery = [
    "username",
    "userType",
    "userNumber",
    "partyNumber",
    "firstNumber",
    "lastNumber",
    "delayBetweenCalls",
];
const {
    username, userType, userNumber, partyNumber,
    firstNumber, lastNumber, delayBetweenCalls }
= initScenarioUserDataFromURL(urlQuery);

declare var window;
declare var Cti;
declare var xc_webrtc;

const isFirstUser: boolean = (firstNumber === userNumber);
let antecedentCalleeNumber: string;
if (!isFirstUser) antecedentCalleeNumber = (parseInt(userNumber) - 1).toString();
initOrIncrementIteration();
window['appVersion'] = `${userType}.${userNumber}`;
window.onbeforeunload = function(){
    xc_webrtc.stop();
    localStorage.clear();
}

function loadCaller(): void {
    Cti.setHandler(Cti.MessageType.LOGGEDON, () => startRegistration());
    (new User(username, userNumber)).login();
    return;
}

function startRegistration(): void {
    Cti.unsetHandler(Cti.MessageType.LOGGEDON, startRegistration);
    Cti.setHandler(Cti.MessageType.PHONEHINTSTATUSEVENT, (e: PhoneHintStatusEvent) => finishRegistration(e));
    attachReporters(username, userNumber, userType, partyNumber);
    Cti.subscribeToPhoneHints([userNumber]);
    getLineConfig(username);
    return;
};

function finishRegistration(e: PhoneHintStatusEvent): void {
    if (e.status === 0 && e.number === userNumber) {
        Cti.unsubscribeFromAllPhoneHints();
        Cti.unsetHandler(Cti.MessageType.PHONEHINTSTATUSEVENT, finishRegistration);
        console.log(`PhoneStatusUpdate: user registered with number ${userNumber}; waiting for last callee to become available: ${lastNumber}.`);
        Cti.setHandler(Cti.MessageType.PHONEHINTSTATUSEVENT, (e: PhoneHintStatusEvent) => whenLastCalleeBecomesAvailable(e));
        Cti.subscribeToPhoneHints([lastNumber]);
        return;
    }
    return;
}

function whenLastCalleeBecomesAvailable(e: PhoneHintStatusEvent): void {
    if (e.status === 0 && e.number === lastNumber) {
        Cti.unsubscribeFromAllPhoneHints();
        Cti.unsetHandler(Cti.MessageType.PHONEHINTSTATUSEVENT, whenLastCalleeBecomesAvailable);
        console.log(`PhoneStatusUpdate: last callee number: ${lastNumber} is available. Starting queue for call establishment`);
        if (isFirstUser) {
            console.log(`PhoneStatusUpdate: I'm the first caller!, calling callee now!`);
            dialCalleeAfter();
            return;
        } else {
            console.log(`PhoneStatusUpdate: waiting for antecedent callee number: ${antecedentCalleeNumber} to be in call.`);
            Cti.setHandler(Cti.MessageType.PHONEHINTSTATUSEVENT, (e: PhoneHintStatusEvent) => waitForAntecedentCall(e));
            Cti.subscribeToPhoneHints([antecedentCalleeNumber]);
            return;
        }
    }
    return;
}

function waitForAntecedentCall(e: PhoneHintStatusEvent): void {
    if (e.status === 1 && e.number === antecedentCalleeNumber) {
        Cti.unsetHandler(Cti.MessageType.PHONEHINTSTATUSEVENT, waitForAntecedentCall);
        Cti.unsubscribeFromAllPhoneHints();
        console.log(`PhoneStatusUpdate: antecedent callee number is in call, after ${delayBetweenCalls} ms will dial callee number ${partyNumber}`);
        dialCalleeAfter();
        return;
    }
    return;
}

function dialCalleeAfter(): void {
    setTimeout(() => {
        Cti.setHandler(Cti.MessageType.PHONEEVENT, (ev: PhoneEvent) => whenCalleeHangup(ev));
        Cti.dial(partyNumber);
        return;
    }, delayBetweenCalls);
}

function whenCalleeHangup(e: PhoneEvent): void {
    if (e.eventType === 'EventReleased') {
        Cti.unsetHandler(Cti.MessageType.PHONEEVENT, whenCalleeHangup);
        console.log('PhoneEvent: Callee hangup, going to hangup too and close line.');
        Cti.hangup();
        xc_webrtc.stop();
        return;
    }
    return;
};

loadCaller();