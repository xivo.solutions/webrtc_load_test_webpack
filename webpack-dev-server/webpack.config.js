const webpack = require('webpack');
const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');

const SCENARIOS = process.env.SCENARIOS;
const TEMPLATE = './index.html';

const createHtmlWebpackPlugin = (scenario, userType) => {
    return new htmlWebpackPlugin({
        chunks: [`${scenario}${userType}`],
        template: TEMPLATE,
        filename: `./${scenario}${userType}.html`,
    });
}

const initWebpack = () => {
    const scenarios = SCENARIOS.split(',');
    
    let webpackInit = {
        webpackEntry: {},
        plugins: [],
    }

    scenarios.forEach(scenario => {
        webpackInit.webpackEntry[`${scenario}Callee`] = `./scenarios/${scenario}/Callee.js`;
        webpackInit.webpackEntry[`${scenario}Caller`] = `./scenarios/${scenario}/Caller.js`;
        webpackInit.plugins.push(createHtmlWebpackPlugin(scenario, 'Callee'));
        webpackInit.plugins.push(createHtmlWebpackPlugin(scenario, 'Caller'));
    })

    return webpackInit;
}

const { webpackEntry, plugins } = initWebpack(); 

module.exports = (env) => {
    return [{
        mode: 'production',
        entry: webpackEntry,
        output: {
            filename: '[name].bundle.js',
            path: path.resolve(__dirname, 'dist'),
        },
        plugins: [
            ...plugins,
            new webpack.DefinePlugin({
                'process.env': {
                    'SUT_FQDN': JSON.stringify(env.SUT_FQDN),
                    'WEBPACK_PORT': JSON.stringify(env.WEBPACK_PORT),
                    'DEFAULT_USER_PWD': JSON.stringify(env.DEFAULT_USER_PWD),
                    'CLIENT_PROXY_PORT': JSON.stringify(env.CLIENT_PROXY_PORT),
                    'LOADTEST_URL': JSON.stringify(env.LOADTEST_URL),
                    'XIVO_IP': JSON.stringify(env.XIVO_IP),
                    'MDS1_IP': JSON.stringify(env.MDS1_IP),
                    'MDS2_IP': JSON.stringify(env.MDS2_IP)
                }
            })
        ],
        devServer: {
            static: [
                {
                    directory: path.resolve(__dirname, './dist'),
                }
            ],
            port: env.WEBPACK_PORT,
        },
    }];
} 